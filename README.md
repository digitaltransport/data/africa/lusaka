# Lusaka

GIS, GTFS and Transit maps for the city of Lusaka's public transport network.

This data was collected by [Smart Solutech](https://www.linkedin.com/in/smart-solutech-investment-limited-78b02a249/?originalSubdomain=zm) and [Transport for Cairo](https://transportforcairo.com/) under a consortium led by [WRI](https://www.wri.org/) in July 2023 for the "‘Low-Carbon Transport Futures in Zambia" project sponsored by [Climate Compatible Growth](https://climatecompatiblegrowth.com/) and funded by Foreign, Commonwealth and Development Office.

## Contents:

- **Datasets**: GIS & GTFS Dataset outputs from the mapping exercise, along with an index excel sheet containing metadata for each layer.

- **Lusaka-Transit-Map**: Stylized map for Lusaka's PT network, meant to be used for passenger information.